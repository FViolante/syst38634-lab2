

public class testClass {
	
	public static void main(String[] args) {
		double[] a1 = {25, 37, 29};
		double[] a2 = {25, 45, 65};
		String s1 = "350";
		findSmallest(a1);
		findAvg(a2);
		findMid(s1);
		
		System.out.println();
		
		System.out.println(vowelCount("w3resource"));
		System.out.println(getCharPos("Java Exercises!" , 10));
		System.out.println(getUniCount("w3rsource.com"));
		
		
	}
	
	public static double findSmallest(double[] arr) {
		double s = arr[0];
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] < s) {
				s = arr[i];
			}
		}
		System.out.println(s);
		return s;
	}
	
	public static double findAvg(double[] arr) {
		double avg = 0;
		for(int i = 0; i < arr.length; i++) {
			avg += arr[i];
		}
		System.out.println(avg/arr.length);
		return avg/arr.length;
	}
	
	public static String findMid(String str) {
		char[] strArr =  str.toCharArray();
		String mid = "";
		if(strArr.length % 2 == 0) {
			mid += (strArr[(strArr.length/2) - 1]);	
			mid += (strArr[strArr.length/2]);	
		} else {
			mid += strArr[Math.round(strArr.length/2)];
		}
		System.out.println(mid);
		return mid;
	}
	
	public static int vowelCount(String str) {
		char[] vowels = {'a', 'e', 'i', 'o', 'u'};
		
		int count = 0;
		
		char[] arr = str.toLowerCase().toCharArray();
		
		for(int i = 0; i < arr.length; i++) {
			for(int j = 0; j < vowels.length; j++) {
				if(arr[i] == vowels[j]) {
					count++;
					break;
				}
			}
		}
		return count;
	}
	
	public static String getCharPos(String str, int pos) {
		char[] arr = str.toCharArray();
		
		return pos + " is " + arr[pos];
		
	}
	
	public static int getUniCount(String str) {
		return str.codePointCount(0, str.length());
	}
	
	
}