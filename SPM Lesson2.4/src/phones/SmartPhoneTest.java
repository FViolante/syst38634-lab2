package phones;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmartPhoneTest {

	@Test
	public void testGetFormattedPrice() {
		SmartPhone s = new SmartPhone("test", 9.99, 1);
		String price = s.getFormattedPrice();
		assertTrue("Does not match the result", price.equals("$9.99"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		SmartPhone s = new SmartPhone("test", 0, 1);
		String price = s.getFormattedPrice();
		assertTrue("Does not match the result", price.equals("$0"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone s = new SmartPhone("test", 0, 1);
		String price = s.getFormattedPrice();
		assertTrue("Does not match the result", price.equals("$0"));
	}
	
	@Test 
	public void testGetFormattedPriceException() {
		SmartPhone s = new SmartPhone("test", 9.123, 1);
		String price = s.getFormattedPrice();
		assertTrue("Does not match the result", price.equals("$9.123"));
	}

	@Test
	public void testSetVersion() throws VersionNumberException {
		SmartPhone s = new SmartPhone("test", 0, 1);
		s.setVersion(1);
		assertTrue("Does not match the result", s.getVersion() == 1);
	}
	
	@Test
	public void testSetVersionBoundaryIn() throws VersionNumberException {
		SmartPhone s = new SmartPhone("test", 0, 4);
		s.setVersion(4);
		assertTrue("Does not match the result", s.getVersion() == 4);
	}
	
	@Test (expected=VersionNumberException.class) 
	public void testSetVersionBoundaryOut() throws VersionNumberException {
		SmartPhone s = new SmartPhone("test", 0, 4.1);
		s.setVersion(4.1);
		
		fail("Version out of range");
	}
	
	@Test (expected=VersionNumberException.class) 
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone s = new SmartPhone("test", 0, 5);
		s.setVersion(5);

		fail("Version out of range");
	}

}
