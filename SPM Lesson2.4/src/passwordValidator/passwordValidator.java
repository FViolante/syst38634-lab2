package passwordValidator;

public class passwordValidator {
	
	private static int MIN_LENGTH = 8;
	
	public static boolean isValidFormatDigits(String pass) {
		int digits = 0;
		for(char c : pass.toCharArray()) {
			if(Character.isDigit(c))
				digits++;
		}
		
		if(digits < 2)
			return false;
		
		return true;
	}
	
	public static boolean isValidUpper(String pass) {
		int upper = 0;
		for(char c : pass.toCharArray()) {
			if(Character.isUpperCase(c))
				upper++;
		}
		
		if(upper < 2)
			return false;
		
		return true;
	}
	
	public static boolean isValidLength(String pass) {
		return pass.trim().length() >= MIN_LENGTH;
	}
}