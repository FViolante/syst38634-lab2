package passwordValidator;

import static org.junit.Assert.*;

import org.junit.Test;

public class passwordValidatorTest {
	
	
	@Test
	public void testIsValidLength() {
		boolean isValid = passwordValidator.isValidLength("1234567890");
		assertTrue("Length is invalid (less than 8)", isValid);
		
	}
	
	@Test
	public void testIsValidLengthException() {
		boolean isValid = passwordValidator.isValidLength("");
		assertFalse("Length is invalid (less than 8)", isValid);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean isValid = passwordValidator.isValidLength("12345678");
		assertTrue("Length is invalid (less than 8)", isValid);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean isValid = passwordValidator.isValidLength("1234567");
		assertFalse("Length is invalid (less than 8)", isValid);
	}
	
	
	
	@Test 
	public void testIsValidFormatDigits() {
		boolean isValid = passwordValidator.isValidFormatDigits("aaaa0000");
		assertTrue("Requires atleast 2 digits", isValid);
	}
	
	@Test 
	public void testIsValidFormatDigitsException() {
		boolean isValid = passwordValidator.isValidFormatDigits("aaaaaaaa");
		assertFalse("Requires atleast 2 digits", isValid);
	}
	
	@Test 
	public void testIsValidFormatDigitsBoundaryIn() {
		boolean isValid = passwordValidator.isValidFormatDigits("aaaaaa00");
		assertTrue("Requires atleast 2 digits", isValid);
	}
	
	@Test 
	public void testIsValidFormatDigitsBoundaryOut() {
		boolean isValid = passwordValidator.isValidFormatDigits("aaaaaaa0");
		assertFalse("Requires atleast 2 digits", isValid);
	}
	
	
	
	public void testIsValidUpper() {
		boolean isValid = passwordValidator.isValidUpper("aaaaAAAA");
		assertTrue("Requires atleast 2 Uppercase", isValid);
	}
	
	@Test 
	public void testIsValidUpperException() {
		boolean isValid = passwordValidator.isValidUpper("aaaaaaaa");
		assertFalse("Requires atleast 2 Uppercase", isValid);
	}
	
	@Test 
	public void testIsValidUpperBoundaryIn() {
		boolean isValid = passwordValidator.isValidUpper("aaaaaaAA");
		assertTrue("Requires atleast 2 Uppercase", isValid);
	}
	
	@Test 
	public void testIsValidUpperBoundaryOut() {
		boolean isValid = passwordValidator.isValidUpper("aaaaaaaA");
		assertFalse("Requires atleast 2 Uppercase", isValid);
	}
}
