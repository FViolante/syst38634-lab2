/**
 * @author Fabrizio Violante
 * 
 * 
 */

package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest extends Time {
	
	@Test
	public void testGetTotalMilliseconds() {
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:01:003");
		assertTrue("The time provided does not match the result", totalMilliseconds == 1003);
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:00:999");
		assertTrue("The time provided does not match the result", totalMilliseconds == 999);
	}
	
	@Test (expected=NumberFormatException.class) 
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:00:1000");
		fail("Time Provided is not valid");
	}
	
	@Test (expected=NumberFormatException.class) 
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("00:00:00:30A");
		fail("Time Provided is not valid");
	}
	
	/*
	@Test
	public void testGetTotalSeconds() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:00");
		assertTrue("The time provided does not match the result", totalSeconds == 0);
	}
	
	@Test (expected=NumberFormatException.class) 
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("25:60:60");
		fail("Time Provided is not valid");
	}
	
	@Test (expected=NumberFormatException.class) 
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("Time Provided is not valid");
	}
	*/
}
